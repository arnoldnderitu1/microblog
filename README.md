# Welcome to Microblog!

Microblogging is an online broadcast medium that exists as a specific form of blogging. A micro-blog differs from a traditional blog in that its content is typically smaller in size.

## Features
* The application has a user-subsystem that allows users to log in and log out. 
* A registration page for users to create new accounts.
* A password recovery option
* A home page where user can publish content and view content of users they follow.
* A profile page for users to edit their info, view followers and thos they follow
* An explore page to view what other users are writting.
* Pagination 
* An intelligent search engine to search through users contents.
* Live translation to translate user's content
* Private messaging
* A user can export and email themselves all their content as a data file. This process runs as a background task without affecting the performance of the application.

## Setting up a development environment
Assuming you have `git` and `virtualenv` and `virtualenvwrapper` installed.

1. Clone the code repo
```
$ git clone https://gitlab.com/arnoldnderitu1/microblog.git
$ cd microblog
```

2. Create `venv` virtual env and activate it
```
$ python3 -m venv venv
$ . venv/bin/activate
```

3. Install required packages
```
$ pip install -r requirements
```

4. Initialize the Database
```
$ flask db upgrade
```

5. Running the app in debug mode
```
$ export FLASK_DEBUG=1
$ flask run
```